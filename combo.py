def if_flesh(suit):  # принимает suits
    """проверка на флеш (5 карт одной масти)
    принимает от 5 до 7 карт"""
    suits = suit
    """заполняем словарь по мастям"""
    dick = {}
    for i in suits:
        if i in dick.keys():
            dick[i] += 1
        else:
            dick[i] = 1
    mast = None
    """ищем 5 одинаковых мастей"""
    for k, v in dick.items():
        if v >= 5:
            mast = k
    if mast is not None:
        return True, mast
    else:
        return False, mast


def if_street(num):  # принимает nums
    """проверка на стрит (5 карт по порядку)
    принимает от 5 до 7 карт"""
    nums = num
    """переходим от букв к цифрам"""
    # 2 3 4 5 6 7 8 9 10  J  Q  K  A
    # 2 3 4 5 6 7 8 9 10 11 12 13 14
    for i in range(len(nums)):
        if nums[i] == 'J':
            nums[i] = 11
        elif nums[i] == 'Q':
            nums[i] = 12
        elif nums[i] == 'K':
            nums[i] = 13
        elif nums[i] == 'A':
            nums[i] = 14
        nums[i] = int(nums[i])
    """сортировка п оубываний"""
    nums.sort(reverse=True)
    """ищем 5 подряд"""
    ind = None
    is_street = False
    for j in range(len(nums) - 4):
        streak = 0
        ind = j
        end = j + 4
        k = j
        while k < end:
            """выход из цикла при выходе за размер списка"""
            if k == len(nums) - 1:
                break
            """обработка одинаковых подряд"""
            if nums[k] == nums[k+1]:
                end += 1
                k += 1
                continue
            if abs(nums[k] - nums[k+1]) == 1:
                streak += 1
            k += 1

        if streak == 4:
            is_street = True
            break
    max_val = nums[ind]
    """переходим от цифк к буквам"""
    # 2 3 4 5 6 7 8 9 10  J  Q  K  A
    # 2 3 4 5 6 7 8 9 10 11 12 13 14
    if max_val == 11:
        max_val = 'J'
    elif max_val == 12:
        max_val = 'Q'
    elif max_val == 13:
        max_val = 'K'
    elif max_val == 14:
        max_val = 'A'
    max_val = str(max_val)

    return is_street, max_val


def higest_card(num):  # принимает nums
    """комбинация одной высшей карты"""
    nums = num
    """переходим от букв к цифрам"""
    # 2 3 4 5 6 7 8 9 10  J  Q  K  A
    # 2 3 4 5 6 7 8 9 10 11 12 13 14
    for i in range(len(nums)):
        if nums[i] == 'J':
            nums[i] = 11
        elif nums[i] == 'Q':
            nums[i] = 12
        elif nums[i] == 'K':
            nums[i] = 13
        elif nums[i] == 'A':
            nums[i] = 14
        nums[i] = int(nums[i])

    max_val = max(nums)
    """переходим от цифк к буквам"""
    # 2 3 4 5 6 7 8 9 10  J  Q  K  A
    # 2 3 4 5 6 7 8 9 10 11 12 13 14
    if max_val == 11:
        max_val = 'J'
    elif max_val == 12:
        max_val = 'Q'
    elif max_val == 13:
        max_val = 'K'
    elif max_val == 14:
        max_val = 'A'
    max_val = str(max_val)

    return max_val


def complicated_ones(suit, num):
    """проверка на флеши и стриты """
    suits = suit
    nums = num

    is_street, max_street = if_street(nums)
    is_flesh, mast = if_flesh(suits)

    if is_flesh:
        if is_street:
            if max_street == 'A':
                # роял
                return 9, max_street, 'роял стрит флеш'
            else:
                # стрит флеш
                return 8, max_street, 'стрит флеш'
        else:
            # флеш
            return 5, mast, 'флеш'
    else:
        if is_street:
            # стрит
            return 4, max_street, 'стрит'
        else:
            # ничего, максимум старшая карта
            return 0, higest_card(nums), 'высшая карта'


def simple_ones(lists):  # принимает cards
    cards = lists
    """вынимаем значения и масти карт"""
    suits = []
    nums = []
    for card in cards:
        suits.append(card.suit)
        nums.append(card.value)

    """заполнение словаря со значениями"""
    dick = {}
    for j in nums:
        if j in dick.keys():
            dick[j] += 1
        else:
            dick[j] = 1
    # print(dick)

    """проверка значений"""
    if 4 in dick.values():
        """выявляем каре"""
        maxim = None
        for k, v in dick.items():
            if v == 4:
                maxim = k
        # print('yes', maxim, 'каре')
        return 7, maxim, 'каре'
    if 3 in dick.values() and 2 in dick.values():
        """выявляем фулл хаус"""
        tri = None
        dva = []
        for k, v in dick.items():
            if v == 3:
                tri = k
            if v == 2:
                dva.append(k)
        # print('yes', tri, max(dva), 'фулл хаус')
        return 6, (tri, max(dva)), 'фулл хаус'
    if list(dick.values()).count(3) == 2:
        """выявляем сет, если две тройки"""
        tri = []
        for k, v in dick.items():
            if v == 3:
                tri.append(k)
        # print('yes', max(tri), 'сет')
        return 3, max(tri), 'сет'
    if list(dick.values()).count(2) == 3:
        """выявляем две пары, если три двойки"""
        dva = []
        for k, v in dick.items():
            if v == 2:
                dva.append(k)
        maxim1 = max(dva)
        dva.remove(maxim1)
        maxim2 = max(dva)
        # print('yes', maxim1, maxim2, 'две пары')
        return 2, (maxim1, maxim2), 'две пары'

    """обрабатываем более сложные комбинации
    где могут быть не только выше перечисленные комбинации"""
    local_komb = None
    local_karta = None
    local_name = None
    if list(dick.values()).count(3) == 1:
        tri = None
        for k, v in dick.items():
            if v == 3:
                tri = k
        # print('yes', tri, 'сет')
        local_komb, local_karta, local_name = 3, tri, 'сет'
    if list(dick.values()).count(2) == 2:
        dva = []
        for k, v in dick.items():
            if v == 2:
                dva.append(k)
        maxim1 = max(dva)
        dva.remove(maxim1)
        maxim2 = max(dva)
        # print('yes', maxim1, maxim2, 'две пары')
        local_komb, local_karta, local_name = 2, (maxim1, maxim2), 'две пары'
    if list(dick.values()).count(2) == 1:
        dva = None
        for k, v in dick.items():
            if v == 2:
                dva = k
        # print('yes', dva, 'пара')
        local_komb, local_karta, local_name = 1, dva, 'пара'
    if len(dick) == len(cards):
        local_komb, local_karta, local_name = 0, higest_card(nums), 'высшая карта'

    if len(dick.values()) < 5:
        return local_komb, local_karta, local_name
    else:
        """если больше 5 разных карт, то проверяем на флеши и стриты"""
        komb, karta, name = complicated_ones(suits, nums)
        if komb > local_komb:
            return komb, karta, name
        else:
            return local_komb, local_karta, local_name


"""
старшая карта - 0
пара - 1
две пары - 2
сет (тройка) - 3
стрит - 4
флеш - 5
фулл хаус - 6
каре - 7
стрит флеш - 8
роял стрит флеш - 9
"""


def combo_solver(lists):
    cards = lists
    return simple_ones(cards)
