import pygame
from random import randint, shuffle


class Player(pygame.sprite.Sprite):
    def __init__(self, name, x, y, image, width, height, card, money):
        super().__init__()
        self.name = name
        self.card = card
        self.money = money
        self.image = pygame.transform.scale(pygame.image.load(image), (width, height))

        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y


class Cards(pygame.sprite.Sprite):
    """Класс кард с полями масть, значение, рубашка и изображение"""
    def __init__(self, suit, value, width, height, x=50, y=50):
        super().__init__()

        self.suit = suit
        self.value = value
        self.width = width
        self.height = height
        self.shirt = pygame.transform.scale(pygame.image.load(r"cards/backs/back2.png"), (self.width, self.height))
        self.image = pygame.transform.scale(pygame.image.load(r"cards/" + str(self.value) + str(self.suit) + ".png"),
                                              (self.width, self.height))

        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x


class Shirt(pygame.sprite.Sprite):
    """класс дял рубашек карт на столе"""
    def __init__(self, x, y, width, height, image, angle):
        super().__init__()
        self.image1 = pygame.transform.rotate(pygame.image.load(image), angle)
        self.image = pygame.transform.scale(self.image1, (width, height))

        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x
        self.angle = angle


class Button:
    """класс для кнопки"""
    def __init__(self, x, y, width, height, text, color, display):

        self.x = x
        self.y = y
        self.height = height
        self.width = width
        self.text = text
        self.color = color
        self.display = display

    def is_on_me(self, pos):
        """проверка курсора"""
        x, y = pos
        if self.x <= x <= self.x + self.width and self.y <= y <= self.y + self.height:
            return True
        else:
            return False

    def show(self):
        """функция для отрисовки"""
        pygame.draw.rect(self.display, self.color, (self.x, self.y, self.width, self.height))


def mixing(cards):
    """Функция удаления карты из списка всех карт и возвращения его"""
    i = randint(0, len(cards) - 1)
    k = cards[i]
    cards = cards[:i] + cards[(i + 1):]
    return k, cards


def distribution(cards):
    """Выдаёт игроку две карты"""
    card1, cards = mixing(cards)
    card1.rect.x = 605
    card1.rect.y = 500
    drown_list.add(card1)
    card2, cards = mixing(cards)
    card2.rect.x = 475
    card2.rect.y = 500
    drown_list.add(card2)
    return cards


# J - валет Q - дама K - король A - туз
numbers = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
# C - крести D - бубны H - черви S - пики
suits = ['C', 'D', 'H', 'S']

"""список всех карт"""
cards = []
"""отрисовываемые карты"""
drown_list = pygame.sprite.Group()

"""отрисовка рубашек"""
drown_backs_list = pygame.sprite.Group()

drown_icons = pygame.sprite.Group()

# Заполнение класса карт
for sui in suits:
    for num in numbers:
        cards.append(Cards(sui, num, 125, 179))
shuffle(cards)  # Перемешивание колоды

"""создание стопки карт"""
for i in range(15, 9, -1):
    shirt = Shirt(i, i, 125, 179, 'cards/backs/back2.png', 0)
    drown_backs_list.add(shirt)
"""создание карт у игроков позже будет создаваться в классе игрока"""
"""левые карты"""
shirt = Shirt(-70, 345, 179, 125, 'cards/backs/back2.png', 90)
drown_backs_list.add(shirt)
shirt = Shirt(-70, 225, 179, 125, 'cards/backs/back2.png', 90)
drown_backs_list.add(shirt)

icon1 = Player(0, 115, 300, 'cards/icons/icon1.jpg', 80, 80, 0, 0)
drown_icons.add(icon1)
"""верхние карты"""
shirt = Shirt(475, -70, 125, 179, 'cards/backs/back2.png', 180)
drown_backs_list.add(shirt)
shirt = Shirt(595, -70, 125, 179, 'cards/backs/back2.png', 180)
drown_backs_list.add(shirt)

icon2 = Player(0, 380, 15, 'cards/icons/icon2.jpg', 80, 80, 0, 0)
drown_icons.add(icon2)
"""правые карты"""
shirt = Shirt(1091, 225, 179, 125, 'cards/backs/back2.png', 270)
drown_backs_list.add(shirt)
shirt = Shirt(1091, 345, 179, 125, 'cards/backs/back2.png', 270)
drown_backs_list.add(shirt)

icon3 = Player(0, 1000, 300, 'cards/icons/icon3.jpg', 80, 80, 0, 0)
drown_icons.add(icon3)

"""словарь комбинаций"""
combo_dict = {0: 'старшая карта',
              1: 'пара',
              2: 'две пары',
              3: 'сет',
              4: 'стрит',
              5: 'флеш',
              6: 'фулл хаус',
              7: 'каре',
              8: 'стрит флеш',
              9: 'роял стрит флеш'}


""" инициализания модуля pygame """
pygame.init()

"""создание экрана"""
display = pygame.display.set_mode((1200, 700))

"""хз"""
FPS = 60
clock = pygame.time.Clock()

"""кнопка"""
button = Button(550, 340, 100, 50, 'push me', (0, 0, 255), display)
button.show()
distribution_num = True  # Контроль выдаваемых карт
"""главный цикл игры"""
done = False
while not done:
    """отрисовка фона"""
    display.fill((00, 94, 00))
    # display.blit(shirt, (15, 15)), display.blit(shirt, (14, 14)), display.blit(shirt, (13, 13))
    # display.blit(shirt, (13, 13)), display.blit(shirt, (12, 12)), display.blit(shirt, (11, 11))
    # display.blit(shirt, (10, 10)), display.blit(shirt, (475, -10)), display.blit(shirt, (595, -10))
    # display.blit(shirt_left, (1031, 225)), display.blit(shirt_left, (1031, 345))
    # display.blit(shirt_left, (-10, 345)), display.blit(shirt_left, (-10, 225))


    # pygame.time.delay(10)
    """отрисовка кнопки"""
    button.show()
    """обработка событий"""
    for e in pygame.event.get():
        # print(e)
        if e.type == pygame.QUIT:
            done = True
        # обработка нажатия книпки мыши
        elif e.type == pygame.MOUSEBUTTONDOWN:
            if button.is_on_me(pygame.mouse.get_pos()):
                if len(cards) != 1:
                    print('yes')  # пишет yes при нажатии на кнопку
                    if distribution_num:
                        cards = distribution(cards)
                        distribution_num = False
        # обработка отпуска кнопки мыши
        elif e.type == pygame.MOUSEBUTTONUP:
            pass

    """обновление всех обьектов"""
    drown_list.update()
    drown_backs_list.update()

    """отрисовка всех обьектов"""
    drown_list.draw(display)
    drown_backs_list.draw(display)
    drown_icons.draw(display)

    """обновление экрана"""
    pygame.display.flip()
    clock.tick(FPS)


pygame.quit()
